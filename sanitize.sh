#/bin/bash

sed -i 's/Tor Aamodt/Tor M. Aamodt/g' $1/*.csv
sed -i 's/Antonio Gonzalez/Antonio González/g' $1/*.csv
sed -i 's/Daniel Jimenez/Daniel A. Jiménez/g' $1/*.csv
sed -i 's/Daniel Jiménez/Daniel A. Jiménez/g' $1/*.csv
sed -i 's/Trevor Carlson/Trevor E. Carlson/g' $1/*.csv
sed -i 's/Emre Ozer/Emre Özer/g' $1/*.csv
sed -i 's/Yiannakis Sazeides/Yanos Sazeides/g' $1/*.csv
sed -i 's/Mary Jane Irwin/Janie Irwin/g' $1/*.csv
sed -i 's/Andre Seznec/André Seznec/g' $1/*.csv
sed -i 's/Moinnudin Qureshi/Moin Qureshi/g' $1/*.csv
sed -i 's/Dan Sorin/Daniel Sorin/g' $1/*.csv
sed -i 's/Ben Lee/Benjamin Lee/g' $1/*.csv
sed -i 's/Engin Ipek/Engin İpek/g' $1/*.csv
sed -i 's/Norm Jouppi/Norman Jouppi/g' $1/*.csv
sed -i 's/Tim Sherwood/Timothy Sherwood/g' $1/*.csv
sed -i 's/Per Stenstrom/Per Stenström/g' $1/*.csv
sed -i 's/Hsien Hsin Lee/Hsien-Hsin Lee/g' $1/*.csv
sed -i 's/Hsien Hsin Sean Lee/Hsien-Hsin Lee/g' $1/*.csv
sed -i 's/Douglas Carmean/Doug Carmean/g' $1/*.csv
sed -i 's/William Dally/Bill Dally/g' $1/*.csv
sed -i 's/John Shen/John P. Shen/g' $1/*.csv
sed -i 's/Bradford Beckmann/Brad Beckmann/g' $1/*.csv
sed -i 's/Guri Sohi/Gurindar S. Sohi/g' $1/*.csv
sed -i 's/Christopher Wilkerson/Chris Wilkerson/g' $1/*.csv
sed -i 's/Moinuddin Qureshi/Moin Qureshi/g' $1/*.csv
sed -i 's/Krste Asanovic/Krste Asanović/g' $1/*.csv

# Institutions
sed -i 's/University of Illinois Urbana-Champaign/University of Illinois at Urbana-Champaign/g' $1/*.csv
sed -i 's/Intel Corporation/Intel/g' $1/*.csv
sed -i 's/Microsoft Corporation/Microsoft/g' $1/*.csv
sed -i 's/Cavium Corporation/Cavium/g' $1/*.csv
sed -i 's/Microsoft Azure/Microsoft/g' $1/*.csv
sed -i 's/University of California, Santa Barbara/University of California at Santa Barbara/g' $1/*.csv
sed -i 's/University of California Santa Barbara/University of California at Santa Barbara/g' $1/*.csv
sed -i 's/University of California, San Diego/University of California at San Diego/g' $1/*.csv
sed -i 's/University of California San Diego/University of California at San Diego/g' $1/*.csv
sed -i 's/University of California, Los Angeles/University of California at Los Angeles/g' $1/*.csv
sed -i 's/University of California Los Angeles/University of California at Los Angeles/g' $1/*.csv
sed -i 's/University of California, Riverside/University of California at Riverside/g' $1/*.csv
sed -i 's/University of California Riverside/University of California at Riverside/g' $1/*.csv
sed -i 's/University of California, Santa Cruz/University of California at Santa Cruz/g' $1/*.csv
sed -i 's/University of California Santa Cruz/University of California at Santa Cruz/g' $1/*.csv
sed -i 's/University of California,Berkeley/University of California at Berkeley/g' $1/*.csv
sed -i 's/Georgia Institute of Technology/Georgia Tech/g' $1/*.csv
sed -i 's/Massachusetts Institute of Technology/MIT/g' $1/*.csv
sed -i 's/William and Mary/William & Mary/g' $1/*.csv
sed -i 's/William&Mary/William & Mary/g' $1/*.csv
sed -i 's/NC State University/North Carolina State University/g' $1/*.csv
sed -i 's/Korea Advanced Institute of Science and Technology/KAIST/g' $1/*.csv
sed -i 's/University of Wisconsin-Madison/University of Wisconsin at Madison/g' $1/*.csv