#!/bin/python3

import os
import pandas as pd
import math

dirs = ["ISCA", "MICRO"]

def allAttendancePerPerson(all_dict):
  names = []
  years = {}

  for name in all_dict.keys():
    names.append(name)
    for year in sorted(all_dict[name]["Years"].keys()):
      if not year in years.keys():
        years[year] = []
      years[year].append(int(all_dict[name]["Years"][year]))

  final_dict = {"Name" : names}
  final_dict.update(years)

  frame = pd.DataFrame.from_dict(final_dict)
  frame = frame.set_index("Name")
  frame["Total"] = frame[[year for year in years.keys()]].sum(axis=1)
  frame = frame.sort_values(by=["Total"], ascending=False)

  return frame

def main():
  for conf in dirs:
    overall_dict = {}
    years = []

    filenames = [filename for filename in os.listdir(conf) if os.path.isfile(os.path.join(conf, filename))]
    filenames = sorted(filenames)

    for filename in filenames:
      print("Processing",filename)
      f = os.path.join(conf, filename)
      year = int(filename.split(".")[0])
      years.append(year)

      # Populate (likely the wrong way)
      year_dict = pd.read_csv(f).to_dict()

      for key in year_dict["Name"].keys():
        name = year_dict["Name"][key]

        if not name in overall_dict.keys():
          overall_dict[name] = {"Years" : {}}

        overall_dict[name]["Years"][year] = True

    for year in years:
      for name in overall_dict.keys():
        if not year in overall_dict[name]["Years"].keys():
          overall_dict[name]["Years"][year] = False

    print(allAttendancePerPerson(overall_dict).to_string())


if __name__ == "__main__":
    main()


